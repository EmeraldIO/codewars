/*You have the radius of a circle with the center in point (0,0).

Write a function that calculates the number of points in the circle where (x,y) - the cartesian coordinates of the points - are integers.

Example: for radius = 2 the result should be 13.

0 <= radius <= 1000*/
public class Kata {
    public static int pointsNumber(int radius){
        int rsqr = (int)Math.pow(radius,2);
        int counter = 0;
        for (int i = 1; i < radius ; i++) {
            for (int j = 1; j < radius ; j++) {
                if (((Math.pow(i,2.0)) +(Math.pow(j,2.0)))<=rsqr) {
                    counter++;
                }
            }
        }
        return (counter*4)+(radius*4)+1;
    }
}