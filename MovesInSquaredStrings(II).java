/*You are given a string of n lines, each substring being n characters long: For example:

s = "abcd\nefgh\nijkl\nmnop"

We will study some transformations of this square of strings.

Clock rotation 180 degrees: rot
rot(s) => "ponm\nlkji\nhgfe\ndcba"
selfie_and_rot(s) (or selfieAndRot or selfie-and-rot) It is initial string + string obtained by clock rotation 180 degrees with dots interspersed in order (hopefully) to better show the rotation when printed.
s = "abcd\nefgh\nijkl\nmnop" --> 
"abcd....\nefgh....\nijkl....\nmnop....\n....ponm\n....lkji\n....hgfe\n....dcba"
or printed:
|rotation        |selfie_and_rot
|abcd --> ponm   |abcd --> abcd....
|efgh     lkji   |efgh     efgh....
|ijkl     hgfe   |ijkl     ijkl....   
|mnop     dcba   |mnop     mnop....
                           ....ponm
                           ....lkji
                           ....hgfe
                           ....dcba
#Task:

Write these two functions rotand selfie_and_rot
and

high-order function oper(fct, s) where

fct is the function of one variable f to apply to the string s (fct will be one of rot, selfie_and_rot)
#Examples:

s = "abcd\nefgh\nijkl\nmnop"
oper(rot, s) => "ponm\nlkji\nhgfe\ndcba"
oper(selfie_and_rot, s) => "abcd....\nefgh....\nijkl....\nmnop....\n....ponm\n....lkji\n....hgfe\n....dcba"
Notes:
The form of the parameter fct in oper changes according to the language. You can see each form according to the language in "Your test cases".
It could be easier to take these katas from number (I) to number (IV)
Forthcoming katas will study other tranformations.

*/

import java.util.function.Function;
class Opstrings1 {
    
    public static String rot(String strng) {
        String gg = horMirror(strng);
        String[] splitedBuf = gg.split("\\n");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i <splitedBuf.length ; i++) {
            builder.append(new StringBuffer(splitedBuf[i]).reverse());
            if(i!=splitedBuf.length-1){
                builder.append("\n");
            }
        }
        return builder.toString();
    }
    public static String selfieAndRot(String strng) {
        String gg = rot(strng);
        String[] strings1 = gg.split("\n");
        String[] strings = strng.split("\n");
        StringBuilder builder = new StringBuilder();
        StringBuilder dotBuilder = new StringBuilder();
        for (int i = 0; i <strings[0].length() ; i++) {
            dotBuilder.append(".");
        }
        for (int i = 0; i <strings.length ; i++) {
            builder.append(strings[i]).append(dotBuilder).append("\n");
        }for (int i = 0; i <strings1.length ; i++) {
            builder.append(dotBuilder).append(strings1[i]);
            if(i!=strings1.length-1){
                builder.append("\n");
            }
        }
        return builder.toString();
    }
    
    public static String oper(Function<String, String> operator, String s) {
        return operator.apply(s);
    }
    
    private static String horMirror(String strng) {
        String[] splitedStr = strng.split("\n");
        StringBuilder finalStr = new StringBuilder();
        for (int i = splitedStr.length - 1; i >= 0; i--) {
            finalStr.append(new StringBuilder(splitedStr[i]));
            if (i != (0)) {
                finalStr.append("\n");
            }
        }
        return finalStr.toString();
    }
}