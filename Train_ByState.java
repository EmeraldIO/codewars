/*Given a string with friends to visit in different states:

ad3="John Daggett, 341 King Road, Plymouth MA
Alice Ford, 22 East Broadway, Richmond VA
Sal Carpenter, 73 6th Street, Boston MA"
we want to produce a result that sorts the names by state and lists the name of the state followed by the name of each person residing in that state (people's names sorted). When the result is printed we get:

Massachusetts
.....^John Daggett 341 King Road Plymouth Massachusetts
.....^Sal Carpenter 73 6th Street Boston Massachusetts
^Virginia
.....^Alice Ford 22 East Broadway Richmond Virginia
Spaces not being always well seen, in the above result ^ means a white space.

Not printed, the resulting string will be:*/
import java.util.*;
public class State {
    
    public static String byState(String str) {
        HashMap<String, String> map = new HashMap<>();
        TreeMap<String, List<String>> mappedNames = new TreeMap<>();
        map.put("AZ", "Arizona");
        map.put("CA", "California");
        map.put("ID", "Idaho");
        map.put("IN", "Indiana");
        map.put("MA", "Massachusetts");
        map.put("OK", "Oklahoma");
        map.put("PA", "Pennsylvania");
        map.put("VA", "Virginia");
        String[] splited = str.split("\n");
        System.out.println(splited.length);
        for (String s: splited) {
            String stateSymbol = s.split(" ")[s.split(" ").length-1];
            s = s.replace(stateSymbol,map.get(stateSymbol));
            s=String.join("",s.split(","));
            mappedNames.computeIfAbsent(stateSymbol, k -> new ArrayList<>()).add(s.replace(stateSymbol,map.get(stateSymbol)));
        }
        StringBuffer buffer = new StringBuffer();
        for (String g: mappedNames.keySet()) {
            buffer.append(map.get(g)).append("\n");
            Collections.sort(mappedNames.get(g));
            for (String s:  mappedNames.get(g)) {
                buffer.append("..... ").append(s).append("\n");
            }
            buffer.append(" ");
        }
        buffer.delete(buffer.length()-2,buffer.length());
        return buffer.toString();
    }
}