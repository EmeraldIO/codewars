/*Write a class function named repeat() that takes two arguments (a string and a long integer), and returns a new string where the input string is repeated that many times.

Example:
Repeater.repeat("a", 5)
should return

"aaaaa"
*/public class Repeater{
  public static String repeat(String string,long n){
  StringBuffer buffer = new StringBuffer();
        for (int i = 0; i <n ; i++) {
            buffer.append(string);
        }
        return buffer.toString();
  }
}