/*You have an array of numbers.
Your task is to sort ascending odd numbers but even numbers must be on their places.

Zero isn't an odd number and you don't need to move it. If you have an empty array, you need to return it.

Example

sortArray([5, 3, 2, 8, 1, 4]) == [1, 3, 2, 8, 5, 4]*/
import java.util.*;
import java.util.stream.Collectors;
public class Kata {
 public static int[] sortArray(int[] array) {
        List<Integer> resArray = Arrays.stream(array).boxed().collect(Collectors.toList());
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1) {
                list.add(array[i]);
            }
        }
        Collections.sort(list);
        int count=0;
        for (int i = 0; i < resArray.size(); i++) {
            if (resArray.get(i) % 2 != 0) {
                resArray.set(i,list.get(count));
                     count++;
            }
        }
        return resArray.stream().mapToInt(i->i).toArray();
    }
}