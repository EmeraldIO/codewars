/*In this kata we want to convert a string into an integer. The strings simply represent the numbers in words.

Examples:

"one" => 1
"twenty" => 20
"two hundred forty-six" => 246
"seven hundred eighty-three thousand nine hundred and nineteen" => 783919
Additional Notes:

The minimum number is "zero" (inclusively)
The maximum number, which must be supported is 1 million (inclusively)
The "and" in e.g. "one hundred and twenty-four" is optional, in some cases it's present and in others it's not
All tested numbers are valid, you don't need to validate them*/
public static int parseInt(String numStr) {
        int sum = 0;
        if (numStr.equals("one million")) {
            return 1000000;
        }
        numStr = numStr.replace("-", "- ");
        HashMap<String, String> map = new HashMap<>();
        map.put("zero", "0");
        map.put("one", "1");
        map.put("two", "2");
        map.put("three", "3");
        map.put("four", "4");
        map.put("five", "5");
        map.put("six", "6");
        map.put("seven", "7");
        map.put("eight", "8");
        map.put("nine", "9");
        map.put("ten", "10");
        map.put("eleven", "11");
        map.put("twelve", "12");
        map.put("thirteen", "13");
        map.put("fourteen", "14");
        map.put("fifteen", "15");
        map.put("sixteen", "16");
        map.put("seventeen", "17");
        map.put("eighteen", "18");
        map.put("nineteen", "19");
        map.put("twenty", "20");
        map.put("thirty", "30");
        map.put("forty", "40");
        map.put("fifty", "50");
        map.put("sixty", "60");
        map.put("seventy", "70");
        map.put("eighty", "80");
        map.put("ninety", "90");
        map.put("twenty-", "2");
        map.put("thirty-", "3");
        map.put("forty-", "4");
        map.put("fifty-", "5");
        map.put("sixty-", "6");
        map.put("seventy-", "7");
        map.put("eighty-", "8");
        map.put("ninety-", "9");
        map.put("hundred", "100");
        map.put("thousand", "1000");

        String[] split = numStr.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String aSplit : split) {
            int buf = 0;
            if (map.get(aSplit) != null) {
                switch (aSplit) {
                    case "hundred":
                        buf = Integer.parseInt(builder.toString()) * Integer.parseInt(map.get("hundred"));
                        sum = sum + buf;
                        builder.setLength(0);
                        break;
                    case "thousand":
                        if (builder.length() == 0) {
                            builder.append("0");
                        }
                        sum = sum + Integer.parseInt(builder.toString());
                        sum = sum * Integer.parseInt(map.get("thousand"));
                        builder.setLength(0);
                        break;
                    default:
                        builder.append(map.get(aSplit));
                        break;
                }

            }
        }
        if (builder.length() == 0) {
            builder.append("0");
        }
        sum = sum + Integer.parseInt(builder.toString());
        return sum;
    }