/*What is your favourite day of the week? Check if it's the most frequent day of the week in the year.

You are given a year as integer (e. g. 2001). You should return the most frequent day(s) of the week in that year. The result has to be a list of days sorted by the order of days in week (e. g. ['Monday', 'Tuesday']). Week starts with Monday.

Input: Year as an int.

Output: The list of most frequent days sorted by the order of days in week (from Monday to Sunday).

Preconditions: Year is between 1 and 9999. Week starts with Monday. Calendar is Gregorian.

Example:

Kata.mostFrequentDays(2427) => {"Friday"}
Kata.mostFrequentDays(2185) => {"Saturday"}
Kata.mostFrequentDays(2860) => {"Thursday", "Friday"}*/
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
public class Kata {
  public static String[] mostFrequentDays(int year) {
    // Implement me! :) 
   int count = 0;
        TreeMap<Integer, List<DayOfWeek>> map1 = new TreeMap<Integer, List<DayOfWeek>>();
            for (DayOfWeek day : DayOfWeek.values()) {
                LocalDate date1 = LocalDate.of(year, 1, 1).with(TemporalAdjusters.firstInMonth(day));
                while ( date1.getYear() == year) {
                    //System.out.printf("%s%n", date1);
                    date1 = date1.with(TemporalAdjusters.next(day));
                    count++;
                }
                map1.computeIfAbsent(count, k -> new ArrayList<>()).add(day);
                //System.out.println(count);
                count=0;
        }
      List<String> result = new ArrayList<>();
         for (DayOfWeek s: map1.get(53)) {
            String temp = s.toString().toLowerCase();
            String cap = temp.substring(0, 1).toUpperCase() + temp.substring(1);
            result.add(cap);
        }
       return result.stream().toArray(String[]::new);
  }
}