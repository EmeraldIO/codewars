/*Given an array, find the int that appears an odd number of times.

There will always be only one integer that appears an odd number of times.*/
import java.util.*;
import java.util.stream.Collectors;
public class FindOdd {
	public static int findIt(int[] a) {
  	List<Integer> list = Arrays.stream(a).boxed().collect(Collectors.toList());
        return list.stream().filter(i -> Collections.frequency(list, i) %2!=0)
                .collect(Collectors.toSet()).iterator().next();
  }
}