/*I thank yvonne-liu for the idea and for the example tests :)

Description:
Encrypt this!

You want to create secret messages which can be deciphered by the Decipher this! kata. Here are the conditions:

Your message is a string containing space separated words.
You need to encrypt each word in the message using the following rules:
The first letter needs to be converted to its ASCII code.
The second letter needs to be switched with the last letter
Keepin' it simple: There are no special characters in input.
Examples:
Kata.encryptThis("Hello") => "72olle"
Kata.encryptThis("good") => "103doo"
Kata.encryptThis("hello world") => "104olle 119drlo"*/
public class Kata {
    public static String encryptThis(String text) {
        StringBuilder buffer = new StringBuilder();
        if (!text.isEmpty()) {
            String[] split = text.split(" ");
             for (int i = 0; i < split.length; i++) {
            if(split[i].equals("")){
                buffer.append(" ");
            }
            else {
                buffer.append((int) split[i].charAt(0));
                if (split[i].length() < 3) {
                    buffer.append(split[i].substring(1));
                } else {
                    buffer.append(split[i].charAt(split[i].length() - 1)).append(split[i], 2, split[i].length() - 1).append(split[i].charAt(1));
                }
                if (i != split.length - 1) {
                    buffer.append(" ");
                }
            }
        }
        }
        return buffer.toString();
  }
}