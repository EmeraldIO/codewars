/*Take the following IPv4 address: 128.32.10.1

This address has 4 octets where each octet is a single byte (or 8 bits).

1st octet 128 has the binary representation: 10000000
2nd octet 32 has the binary representation: 00100000
3rd octet 10 has the binary representation: 00001010
4th octet 1 has the binary representation: 00000001
So 128.32.10.1 == 10000000.00100000.00001010.00000001

Because the above IP address has 32 bits, we can represent it as the unsigned 32 bit number: 2149583361

Complete the function that takes an unsigned 32 bit number and returns a string representation of its IPv4 address.

Examples
2149583361 ==> "128.32.10.1"
32         ==> "0.0.0.32"
0          ==> "0.0.0.0"*/
/*this one is not my but it is lit
public class Kata {
  public static String longToIP(long ip) {
    return String.format("%d.%d.%d.%d", ip>>>24, (ip>>16)&0xff, (ip>>8)&0xff, ip&0xff);
  }
}
*/
import java.util.*;
import java.util.stream.Collectors;
public class Kata {
  public static String longToIP(long ip) {
    String str = Long.toBinaryString(ip);
        if (str.length() != 32) {
            StringBuilder buffer = new StringBuilder(str);
            for (int i = buffer.length(); i < 32; i++) {
                buffer.insert(0, "0");
            }
            str = buffer.toString();
        }
        int divisor = 8;
        List<String> list = new ArrayList<>();
        while (str.length() > 0) {
            String nextChunk = str.substring(0, divisor);
            list.add(String.valueOf((Long.parseLong(nextChunk, 2))));
            str = str.substring(divisor);
        }
        return (String.join(".", list));
  }
}