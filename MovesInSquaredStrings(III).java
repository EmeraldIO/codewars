/*You are given a string of n lines, each substring being n characters long: For example:

s = "abcd\nefgh\nijkl\nmnop"

We will study some transformations of this square of strings.

Symmetry with respect to the main diagonal: diag_1_sym (or diag1Sym or diag-1-sym)
diag_1_sym(s) => "aeim\nbfjn\ncgko\ndhlp"
Clockwise rotation 90 degrees: rot_90_clock (or rot90Clock or rot-90-clock)
rot_90_clock(s) => "miea\nnjfb\nokgc\nplhd"
selfie_and_diag1(s) (or selfieAndDiag1 or selfie-and-diag1) It is initial string + string obtained by symmetry with respect to the main diagonal.
s = "abcd\nefgh\nijkl\nmnop" --> 
"abcd|aeim\nefgh|bfjn\nijkl|cgko\nmnop|dhlp"
or printed for the last:
selfie_and_diag1
abcd|aeim
efgh|bfjn
ijkl|cgko 
mnop|dhlp
#Task:

Write these functions diag_1_sym, rot_90_clock, selfie_and_diag1
and

high-order function oper(fct, s) where

fct is the function of one variable f to apply to the string s (fct will be one of diag_1_sym, rot_90_clock, selfie_and_diag1)
#Examples:

s = "abcd\nefgh\nijkl\nmnop"
oper(diag_1_sym, s) => "aeim\nbfjn\ncgko\ndhlp"
oper(rot_90_clock, s) => "miea\nnjfb\nokgc\nplhd"
oper(selfie_and_diag1, s) => "abcd|aeim\nefgh|bfjn\nijkl|cgko\nmnop*/

import java.util.function.Function;
class Opstrings {
    
    public static String diag1Sym(String strng) {
        String[] s2 = strng.split("\n");
        int len = s2[0].length();
        StringBuffer[] buf = new StringBuffer[len];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = new StringBuffer();
        }
        for (int i = 0; i < s2.length; i++) {
            for (int j = 0; j < s2[i].length(); j++) {
                buf[j].append(s2[i].charAt(j));
                if (i == s2.length - 1) {
                    s2[j] = buf[j].toString();
                }
            }
        }
        return String.join("\n", s2);
    }
    public static String rot90Clock(String strng) {
         String[] s2 = strng.split("\n");
        int len = s2[0].length();
        StringBuffer[] buf = new StringBuffer[len];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = new StringBuffer();
        }
        for (int i = 0; i < s2.length; i++) {
            for (int j = 0; j < s2[i].length(); j++) {
                buf[j].append(s2[i].charAt(j));
                if (i == s2.length - 1) {
                    s2[j] = buf[j].toString();
                }
            }
        }
        String jj =  String.join("\n", s2);
        return vertMirror(jj);
    }
    public static String selfieAndDiag1(String strng) {
        String[] splited = strng.split("\n");
        String[] splited2 = diag1Sym(strng).split("\n");
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i <splited.length ; i++) {
            builder.append(splited[i]).append("|").append(splited2[i]);
            if(i!=splited.length-1){
                builder.append("\n");
            }
        }
        return builder.toString();
    }
    public static String oper(Function<String, String> operator, String s) {
        return operator.apply(s);
    }
    public static String vertMirror(String strng) {
        String[] splitedStr = strng.split("\n");
        StringBuilder finalStr = new StringBuilder();
        for (int i = 0; i < splitedStr.length; i++) {
            finalStr.append(new StringBuilder(splitedStr[i]).reverse());
            if (i != (splitedStr.length - 1)) {
                finalStr.append("\n");
            }
        }
        return finalStr.toString();
    }
}
