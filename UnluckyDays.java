/*Friday 13th or Black Friday is considered as unlucky day. Calculate how many unlucky days are in the given year.

Find the number of Friday 13th in the given year.

Input: Year as an integer.

Output: Number of Black Fridays in the year as an integer.

Examples:

unluckyDays(2015) == 3
unluckyDays(1986) == 1*/
import java.time.LocalDate;
public class Kata {
  public static int unluckyDays(int year) {
      LocalDate date = LocalDate.of(year,1,1);
        int month = date.getMonthValue();
        int count = 0;
        for (int currentMonth = month; currentMonth <= 12; currentMonth++) {
            date = date.withMonth(currentMonth);
            LocalDate firstDay = date.withDayOfMonth(13);
            if(firstDay.getDayOfWeek().toString().equals("FRIDAY")){
                count++;
            }
        }
       return count;
  }
}