/*Snail Sort
Given an n x n array, return the array elements arranged from outermost elements to the middle element, traveling clockwise.

array = [[1,2,3],
         [4,5,6],
         [7,8,9]]
snail(array) #=> [1,2,3,6,9,8,7,4,5]
For better understanding, please follow the numbers of the next array consecutively:

array = [[1,2,3],
         [8,9,4],
         [7,6,5]]
snail(array) #=> [1,2,3,4,5,6,7,8,9]
This image will illustrate things more clearly:*/
import java.util.ArrayList;
import java.util.List;
public class Snail {

    public static int[] snail(int[][] array) {
    if(array[0].length == 0){
    return new int[]{};
    }
    List<Integer> list = new ArrayList<>();
             int X = array.length;
    while (X>0) {
            for (int k = 0; k < 4; k++) {
                for (int i = 0; i < X; i++) {
                    list.add(array[k][i + k]);
                }
                for (int i = 1; i < X; i++) {
                    list.add(array[i + k][X - 1 + k]);
                }

                for (int i = X - 2; i > 0; i--) {
                    list.add(array[X - 1+k][i+k]);
                }
                for (int i = X - 1; i > 0; i--) {
                    list.add(array[i + k][k]);
                }
                X=X-2;
            }
        }
        return list.stream().mapToInt(i->i).toArray();
        
   } 
}